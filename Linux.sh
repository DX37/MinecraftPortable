#!/bin/bash

set -e

n=""
dir=""
args='-Dtlauncher.bootstrap.targetJar=exec/legacy.jar -Dtlauncher.bootstrap.targetLibFolder=exec/lib'
portable_jvm_version=`cat exec/Java/release`
java_binary=java
primus=

launch()
{
    echo "Starting Bootstrap..."
    echo
    $primus $java_binary $args -jar exec/bootstrap.jar --directory $dir --settings "etc/launcher.properties" &> /dev/null
    saver
}

wipe()
{
    rm -rf ~/.tlauncher
    echo "Removing extracted game..."
    rm -rf $dir
    if [ $java_binary == "/tmp/JRE/bin/java" ]
    then
        echo "Removing portable JRE..."
        rm -rf /tmp/JRE
    fi
    echo "Done!"
}

saver()
{
    clear
    echo "==== What do you want now? ===="
    echo "1 - launch bootstrap again"
    echo "2 - save game, wipe and exit"
    echo "3 - wipe game and exit"
    echo "0 - exit"
    echo
    echo -n "Enter number of menu: "
    read n
    if [ $n == 1 ]
    then
        clear
        launch
        exit
    elif [ $n == 2 ]
    then
        clear
        if [ -e $dir/assets/virtual ]
        then
            echo "Removing virtual assets..."
            rm -rf $dir/assets/virtual
        fi
        echo "==== Saving client changes ===="
        exec/7zip/7zzs u -ms=off -mx=9 -up1q0r2x1y2z1w2 saves.7z $dir/home $dir/launcher_profiles.json $dir/tlauncher_profiles.json
        rm -rf $dir/home
        rm -f $dir/launcher_profiles.json $dir/tlauncher_profiles.json
        clear
        echo "==== Saving client base ===="
        exec/7zip/7zzs u -ms=off -mx=9 -up1q0r2x1y2z1w2 client.7z $dir/*
        clear
        wipe
        exit
    elif [ $n == 3 ]
    then
        clear
        wipe
        exit
    elif [ $n == 0 ]
    then
        clear
        exit
    else
        saver
        clear
        exit
    fi
}

menu()
{
    clear
    if [ -x /usr/bin/primusrun ]
    then
        echo "primusrun detected"
    fi
    echo
    echo "1. Local"
    echo "2. Temp"
    if [ $java_binary == "/tmp/JRE/bin/java" ]
    then
        echo "3. Portable JRE $portable_jvm_version [X]"
    else
        echo "3. Portable JRE $portable_jvm_version [ ]"
    fi

    if [ -x /usr/bin/primusrun ]
    then
        if [ "$primus" == "primusrun" ]
        then
            echo "4. Use primusrun [X]"
        else
            echo "4. Use primusrun [ ]"
        fi
    fi
    echo "0. Exit"
    echo
    echo -n "Enter number of menu: "
    read n
    if [ $n == 1 ]
    then
        dir="./client"
    elif [ $n == 2 ]
    then
        dir="/tmp/Minecraft"
    elif [ $n == 3 ]
    then
        clear
        if [ $java_binary == "java" ]
        then
            java_binary=/tmp/JRE/bin/java
        elif [ $java_binary == "/tmp/JRE/bin/java" ]
        then
            java_binary=java
        fi
        menu
    elif [ $n == 4 -a -x /usr/bin/primusrun ]
    then
        clear
        if [ "$primus" == "" ]
        then
            primus=primusrun
        elif [ "$primus" == "primusrun" ]
        then
            primus=
        fi
        menu
    elif [ $n == 0 ]
    then
        clear
        exit
    else
        menu
        clear
        exit
    fi
}

menu
clear

if [ -e $dir ]
then
    echo "Wiping existing $dir directory..."
    rm -rf $dir
    echo
fi

echo "==== Extracting Minecraft to $dir directory ===="
exec/7zip/7zzs x -o$dir client.7z
clear

if [ -e saves.7z ]
then
	echo "==== Extracting saved changes to client ===="
	exec/7zip/7zzs x -o$dir saves.7z
	clear
fi
if [ $java_binary == "/tmp/JRE/bin/java" ]
then
    echo "==== Extracting portable $(uname -m) JRE to /tmp ===="
    exec/7zip/7zzs x -o/tmp/JRE exec/Java/Linux/$(uname -m).7z
    chmod +x /tmp/JRE/bin/*
    clear
fi
launch
