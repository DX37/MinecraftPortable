@echo off
set n=""
set dir=""
for /f %%i in ('type exec\Java\release') do set portable_jvm_version=%%i
set java_binary="java"
set args=-Dtlauncher.bootstrap.targetJar=exec\legacy.jar -Dtlauncher.bootstrap.targetLibFolder=exec\lib

:start
echo 1. Local
echo 2. Temp
if %java_binary% == "%TEMP%\JVM\bin\java" (
    echo 3. Portable JVM %portable_jvm_version% [X]
) else (
    echo 3. Portable JVM %portable_jvm_version% [ ]
)
echo 0. Exit
echo.
set /p n="Enter number of menu: "
if %n% == 1 (
    set dir=".\client"
    goto unpack
) else if %n% == 2 (
    set dir="%TEMP%\Minecraft"
    goto unpack
) else if %n% == 3 (
    if %java_binary% == "java" set java_binary="%TEMP%\JVM\bin\java"
    if %java_binary% == "%TEMP%\JVM\bin\java" set java_binary="java"
    cls
    goto start
) else if %n% == 0 (
    exit
) else (
    cls
    goto start
)

:unpack
cls
if exist %dir% (
    echo Wiping existing %dir% directory...
    rd /s /q %dir%
    echo.
)

echo ==== Extracting Minecraft to %dir% directory ====
exec\7zip\7za.exe x -o%dir% client.7z
cls

if exist saves.7z (
    echo ==== Extracting saved changes to client ====
    exec\7zip\7za.exe x -o%dir% saves.7z
    cls
)
if %java_binary% == "%TEMP%\JVM\bin\java" (
    echo ==== Extracting Portable %PROCESSOR_ARCHITECTURE% Java to TEMP ====
    exec\7zip\7za.exe x -o%TEMP%\JVM exec\Java\Windows\%PROCESSOR_ARCHITECTURE%.7z
    cls
)

:again
echo Starting Bootstrap...
echo.
%java_binary% %args% -jar exec\bootstrap.jar --directory %dir% --settings "etc/launcher.properties" >nul 2>&1
cls

:saver
echo ==== What do you want now? ====
echo 1 - launch bootstrap again
echo 2 - save game, wipe and exit
echo 3 - wipe game and exit
echo 0 - exit
echo.
set /p n="Enter number of menu: "
if %n% == 1 (
    cls
    goto again
) else if %n% == 2 (
    cls
    goto save
) else if %n% == 3 (
    cls
    goto wipe
) else if %n% == 0 (
    exit
) else (
    cls
    goto saver
)

:save
if exist %dir%\assets\virtual (
    echo Removing virtual assets...
    rd /s /q %dir%\assets\virtual
)
echo ==== Saving client changes ====
exec\7zip\7za.exe u -ms=off -mx=9 -up1q0r2x1y2z1w2 saves.7z %dir%\home %dir%\launcher_profiles.json %dir%\tlauncher_profiles.json
rd /s /q %dir%\home
erase %dir%\launcher_profiles.json %dir%\tlauncher_profiles.json
cls
echo ==== Saving client base ====
exec\7zip\7za.exe u -ms=off -mx=9 -up1q0r2x1y2z1w2 client.7z %dir%\*
cls

:wipe
rd /s /q %APPDATA%\.tlauncher
echo Removing extracted game...
rd /s /q %dir%
if %java_binary% == "%TEMP%\JVM\bin\java" (
    echo Removing JVM...
    rd /s /q %TEMP%\JVM
)
echo Done!
